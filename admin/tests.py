
# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at:
#
#   https://spdx.org/licenses/EUPL-1.1.html
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the Licence for the specific language governing
# permissions and limitations under the Licence.

import time

from User_Interface import User_Interface

default_timeout = 2000

default_message = '''A king there was in days of old
ere man yet walked upon the mould.
His power was reared in cavern's shade,
his hand was over glen and glade.
Of leaves his grown, his mantle green,
his silver lances long and keen.
The starlight in his shield was caught
ere moon was made or sun was wrought.'''

default_steps = [
  'Wenn Sie – vom Hauptbahnhof in München – mit zehn Minuten – ohne dass Sie am'
  ' Flughafen noch einchecken müssen, dann starten Sie im Grunde genommen am Flughafen',
  '– am am Hauptbahnhof in München, starten Sie ihren Flug – zehn Minuten schauen'
  ' Sie sich mal die großen Flughäfen an, wenn Sie in Heathrow in London oder sonstwo',
  ' meine s Charles de Gaulle in äh Frankreich oder in ah in in Rom wenn Sie sich'
  ' mal die Entfernungen ansehen, wenn Sie Frankfurt sich ansehen,',
  'dann werden Sie feststellen, dass zehn Minuten – Sie jederzeit locker in Frankfurt'
  ' brauchen um ihr Gate zu finden – Wenn Sie vom Flug- vom vom Hauptbahnhof starten',
  '– Sie steigen in den Hauptbahnhof ein, Sie fahren mit dem Transrapid in zehn'
  ' Minuten an den Flughafen in an den Flughafen Franz-Josef Strauss dann'
  ' starten Sie praktisch',
  'hier am Hauptbahnhof in München – das bedeutet natürlich dass der Hauptbahnhof'
  ' im Grunde genommen näher an Bayern an die bayerischen Städte heranwächst, weil das ja klar ist,'
  'weil aus dem Hauptbahnhof viele Linien aus Bayern zusammenlaufen.'
]


def exhaustive_with_timeout(ui: User_Interface) -> None:

    # common messages
    ui.warning(default_message, timeout = default_timeout)
    ui.critical(default_message, timeout = default_timeout)
    ui.question(default_message, timeout = default_timeout, default = True)

    # selection
    ui.selection('Dumme Frage', ' '.join(default_message.split('\n')),
      [('s', 'Stimme voll zu!'), ('v', 'Stimme völlig zu!')], [False, True], True, default_timeout)

    # notification
    ui.notify('Dies ist ein wichtiger Hinweis!', timeout = 2000)
    ui.notify('Dies ist ein ganz wichtiger Hinweis!')

    # stepper with extraordinary width
    with ui.stepper(default_steps, 'Erledigt!') as stepper:
        for _ in enumerate(default_steps):
            stepper.announce()
            time.sleep(0.3)
        stepper.announce()
        time.sleep(0.3)

    # stepper with extraordinary width and explicit minimum width
    with ui.stepper(default_steps, 'Erledigt!', minimum_width = 1600) as stepper:
        for _ in enumerate(default_steps):
            stepper.announce()
            time.sleep(0.3)
        stepper.announce()
        time.sleep(0.3)

    # stepper without conclusion but with subtitle
    with ui.stepper(default_steps, subtitle = 'Eine legendäre Rede:') as stepper:
        for _ in enumerate(default_steps):
            stepper.announce()
            time.sleep(0.3)

    # ticker with latency
    with ui.ticker(initial_latency = 1000, intermediate_latency = 200) as ticker:
        for i in range(400000):
            ticker.message('Step %i' % (i + 1))
            time.sleep(0.00000001)

    # ticker with extraordinary width
    with ui.ticker() as ticker:
        for step in default_steps:
            ticker.message(step)
            time.sleep(1)

    # ticker with extraordinary width and explicit minimum width
    with ui.ticker(minimum_width = 1600) as ticker:
        for step in default_steps:
            ticker.message(step)
            time.sleep(1)
