
"""Implicit switch between gui and cli."""

# Author: 2012 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at:
#
#   https://spdx.org/licenses/EUPL-1.1.html
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['run_with']


from typing import TypeVar, ParamSpec, Concatenate
from collections.abc import Callable
import subprocess

from .Abstract_User_Interface import User_Interface
from .Gui_Interface import Gui_Application
from .Cli_Interface import Cli_Interface


A = TypeVar('A')
P = ParamSpec('P')


def gui_available() -> bool:

    proc = subprocess.Popen(['python3', '-c',
      'from PyQt5.QtWidgets import QApplication; QApplication.setSetuidAllowed(True); QApplication([])'],
      stdout = subprocess.PIPE, stderr = subprocess.PIPE)
        # self-experiment detection is more robust wrt. to future developments like Wayland etc.  # noqa: E116
        # than using X-specific things like xdpyinfo etc.                                         # noqa: E116
    proc.communicate()  # dispose any output
    exitcode = proc.wait()
    return exitcode == 0


def run_with(app_name: str, app_title: str, icon_name: str,
  f: Callable[Concatenate[User_Interface, P], A], *args: P.args, **kwargs: P.kwargs) -> A:

    if gui_available():
        gui_app = Gui_Application(app_name, app_title, icon_name)
        return gui_app.event_loop(f, *args, **kwargs)
    else:
        cli_app = Cli_Interface()
        return f(cli_app, *args, **kwargs)
