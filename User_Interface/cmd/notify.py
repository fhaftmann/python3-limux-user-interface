
'''Issues desktop notification.'''

# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at:
#
#   https://spdx.org/licenses/EUPL-1.1.html
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['cmd']


import argparse
import argcomplete

from ..Gui_Interface import Gui_Application, Gui_Interface


app_name = 'notify'

parser = argparse.ArgumentParser(description = __doc__.strip())

parser.add_argument('title', help = 'title of the notification')
parser.add_argument('text', help = 'text of the notification')
parser.add_argument('icon', help = 'xdg icon name for the notification')
parser.add_argument('--timeout', type = int, help = 'optional timeout')


def cmd() -> None:

    argcomplete.autocomplete(parser)

    args = parser.parse_args()

    def main_loop(ui: Gui_Interface) -> None:

        ui.notify(args.text, timeout = args.timeout, title = args.title)

    app = Gui_Application(app_name, args.title, args.icon)
    app.event_loop(main_loop)
