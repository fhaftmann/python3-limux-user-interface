
'''Password dialog as process combinator.'''

# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at:
#
#   https://spdx.org/licenses/EUPL-1.1.html
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['cmd']


from typing import Optional
import argparse
import argcomplete
import resource
import subprocess

from .. import User_Interface, dual_user_interface


app_name = 'supply-password'


def suppress_coredump() -> None:

    resource.setrlimit(resource.RLIMIT_CORE, (0, 0))


parser = argparse.ArgumentParser(description = __doc__.strip())

parser.add_argument('--cancellable', metavar = 'RETURN CODE',
  nargs = '?', const = 0, type = int,
  help = 'allow cancellation, optionally yielding a specified return code')
parser.add_argument('title',
  help = 'title for the password dialog')
parser.add_argument('text',
  help = 'prompt of the password query')
parser.add_argument('icon',
  help = 'xdg icon name for the password query')
parser.add_argument('cmd',
  help = 'command to pass password to via stdin')
parser.add_argument('arg', nargs = argparse.REMAINDER,
  help = 'argument(s) for command')


def cmd() -> None:

    argcomplete.autocomplete(parser)
    args = parser.parse_args()

    def query_password(ui: User_Interface) -> Optional[str]:
        return ui.password(args.text, args.cancellable is not None)

    suppress_coredump()

    password: Optional[str] = dual_user_interface.run_with(app_name, args.title, args.icon, query_password)

    if password is None:
        raise SystemExit(args.cancellable)

    result = subprocess.run([args.cmd] + args.arg, input = password, text = True)

    raise SystemExit(result.returncode)
