
'''Opens minimal browser window.'''

# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at:
#
#   https://spdx.org/licenses/EUPL-1.1.html
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['cmd']


import argparse
import argcomplete

from ..Gui_Interface import Gui_Application, Gui_Interface
from ..gui import minimal_browser


app_name = 'minimal-browser'

parser = argparse.ArgumentParser(description = __doc__.strip())

parser.add_argument('title', help = 'title of the browser window')
parser.add_argument('url', help = 'url to start at')
parser.add_argument('icon', help = 'xdg icon name for the browser window')


def cmd() -> None:

    argcomplete.autocomplete(parser)
    args = parser.parse_args()

    def main_loop(ui: Gui_Interface) -> None:

        minimal_browser.run(ui, args.url)

    app = Gui_Application(app_name, args.title, args.icon)
    app.event_loop(main_loop)
