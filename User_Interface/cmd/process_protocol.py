
'''Running a process with output to a text window with possible abort.'''

# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at:
#
#   https://spdx.org/licenses/EUPL-1.1.html
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['cmd']


from collections.abc import Sequence
import argparse
import argcomplete

from .. import User_Interface
from ..gui.Worker import Worker, Worker_Application
from ..gui.process_protocol import Process_Protocol, Process_Queue


app_name = 'process-protocol'

parser = argparse.ArgumentParser(description = __doc__.strip())

parser.add_argument('--monospace', action = 'store_true',
  help = 'prefer monospace font')
parser.add_argument('title',
  help = 'title for the window')
parser.add_argument('icon',
  help = 'xdg icon name for the window')
parser.add_argument('cmd',
  help = 'command to run whose output is to be displayed in the window')
parser.add_argument('arg', nargs = argparse.REMAINDER,
  help = 'argument(s) for command')


def execute(ui: User_Interface, worker: Worker[int], dialog: Process_Protocol, /,
  cmd: str, args: Sequence[str], monospace: bool) -> int:

    if monospace:
        dialog.set_monospace()

    queue = Process_Queue(worker.aborted, cmd, args)

    while True:
        some_line = queue.get()
        if some_line is None:
            break
        dialog.announce_protocol(some_line)

    return queue.returncode()


def cmd() -> None:

    argcomplete.autocomplete(parser)
    args = parser.parse_args()

    Worker_Application.execute(app_name, args.title, args.icon,
      Process_Protocol, execute, args.cmd, args.arg, args.monospace)  # type: ignore
