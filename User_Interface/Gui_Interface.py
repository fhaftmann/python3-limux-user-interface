
"""Graphical user interface -- Qt via PyQt."""

# Author: 2012 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at:
#
#   https://spdx.org/licenses/EUPL-1.1.html
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['Gui_Application', 'Gui_Interface']


from typing import Optional, TypeVar, Generic
from collections.abc import Iterator, Sequence
from contextlib import contextmanager
from threading import Thread

from PyQt5.QtCore import Qt, QSize, QTimer
from PyQt5.QtGui import QIcon, QKeySequence, QShowEvent, QCloseEvent
from PyQt5.QtWidgets import QApplication, QLabel, QDialog, QVBoxLayout, QHBoxLayout, \
  QPushButton, QRadioButton, QGroupBox, QProgressBar, QMessageBox

from .Abstract_User_Interface import Ticker, Stepper, User_Interface
from .util.Value_Ticker import Value_Ticker
from .gui import base as gui
from .gui import password_dialog, notification


A = TypeVar('A')


simple_notification_flush_timeout = 30 * 1000


class Timeout_Messagebox(QMessageBox):

    def __init__(self, txt: str, timeout: Optional[int], title: str, icon: QMessageBox.Icon | str) -> None:

        super(Timeout_Messagebox, self).__init__(QMessageBox.Icon.NoIcon, title, txt, QMessageBox.StandardButton.Ok)
        if isinstance(icon, QMessageBox.Icon):
            self.setIcon(icon)
        elif isinstance(icon, str):
            self.setIconPixmap(QIcon.fromTheme(icon).pixmap(64, 64))
        self.timeout = timeout

    def showEvent(self, evt: Optional[QShowEvent]) -> None:

        if self.timeout:
            QTimer.singleShot(self.timeout, self.accept)


class Timeout_Questionbox(QMessageBox):

    def __init__(self, txt: str, timeout: Optional[int], title: str, default: bool = False) -> None:

        super(Timeout_Questionbox, self).__init__(QMessageBox.Icon.Question, title, txt,
          QMessageBox.StandardButton.Yes | QMessageBox.StandardButton.No)
        self.timeout = timeout
        self.default = default

    def showEvent(self, evt: Optional[QShowEvent]) -> None:

        if self.timeout:
            QTimer.singleShot(self.timeout, self.accept if self.default else self.reject)


class Timeout_Selectiondialog(QDialog, Generic[A]):

    def __init__(self, heading: str, description: str, descriptions: Sequence[tuple[str, str]],
      values: Sequence[A], default: A, timeout: Optional[int], title: str) -> None:

        assert all(len(description[0]) == 1 for description in descriptions)
        assert default in values

        QDialog.__init__(self)
        self.setWindowTitle(title)
        self.setWindowModality(Qt.WindowModality.NonModal)

        self.button_box = QGroupBox(heading, self)
        box_layout = QVBoxLayout()
        self.button_box.setLayout(box_layout)
        self.description_label = QLabel(description, self.button_box)
        self.description_label.setTextFormat(Qt.TextFormat.PlainText)
        self.description_label.setWordWrap(True)
        box_layout.addWidget(self.description_label)

        self.select_buttons = []
        for selector, description in descriptions:
            button = QRadioButton(self.button_box)
            button.setText(description)
            button.setShortcut(QKeySequence('Alt+' + selector))
            button.setChecked(False)
            box_layout.addWidget(button)
            self.select_buttons.append(button)
        self.select_buttons[values.index(default)].setChecked(True)

        layout_push_buttons = QHBoxLayout()
        self.ok = QPushButton(self)
        self.ok.setText('&OK')
        self.ok.setShortcut(QKeySequence('Alt+O'))
        layout_push_buttons.addWidget(self.ok)
        self.cancel = QPushButton(self)
        self.cancel.setText('&Abbrechen')
        self.cancel.setShortcut(QKeySequence('Alt+A'))
        layout_push_buttons.addWidget(self.cancel)

        layout = QVBoxLayout(self)
        layout.addWidget(self.button_box)
        layout.addItem(layout_push_buttons)

        self.ok.clicked.connect(self.accept)
        self.cancel.clicked.connect(self.reject)

        self.values = values
        self.timeout = timeout
        self.default = default

    def showEvent(self, evt: Optional[QShowEvent]) -> None:

        if self.timeout:
            QTimer.singleShot(self.timeout, self.accept)

    def run(self) -> A:

        result = self.exec()
        if result == QDialog.DialogCode.Accepted:
            for index, button in enumerate(self.select_buttons):
                if button.isChecked():
                    return self.values[index]
        return self.default


class Ticker_Dialog(QDialog):

    def reject(self) -> None:

        pass  # suppress standard interpretation of ESC

    def closeEvent(self, event: Optional[QCloseEvent]) -> None:

        if event is not None:
            event.ignore()


class Gui_Ticker(Ticker):

    def __init__(self, initial_latency: Optional[int], intermediate_latency: Optional[int],
      title: str, minimum_width: Optional[int]) -> None:

        self.initial_latency = initial_latency or 0
        self.intermediate_latency = intermediate_latency or 0

        self.value_ticker: Value_Ticker[Optional[str]] = Value_Ticker()

        self.first_message = False
        self.displayer = Thread(target = self.process_display)
        self.setup(title, minimum_width)

    @gui.thread
    def setup(self, title: str, minimum_width: Optional[int]) -> None:

        self.dialog = Ticker_Dialog()
        layout = QHBoxLayout()
        self.dialog.setWindowTitle(title)
        self.dialog.setWindowModality(Qt.WindowModality.NonModal)

        self.label = QLabel(self.dialog)
        self.label.setTextFormat(Qt.TextFormat.PlainText)
        if minimum_width:
            self.label.setMinimumSize(QSize(minimum_width, self.label.minimumSize().height()))
        layout.addWidget(self.label)

        self.dialog.setLayout(layout)
        gui.adjust_and_center(self.dialog)

    @gui.thread
    def appear(self) -> None:

        self.dialog.show()

    @gui.thread
    def display(self, txt: str) -> None:

        with gui.recentering(self.dialog):
            self.label.setText(txt)
            self.dialog.adjustSize()

    @gui.thread
    def disappear(self) -> None:

        self.dialog.done(0)

    def process_display(self) -> None:

        txt = self.value_ticker.get(self.initial_latency / 1000.0)
        if txt is None:
            return
        self.appear()
        while txt is not None:
            self.display(txt)
            txt = self.value_ticker.get(self.intermediate_latency / 1000.0)
        self.disappear()

    def message(self, txt: str) -> None:

        if not self.first_message:
            self.first_message = True
            self.displayer.start()
        self.value_ticker.put(txt)

    def finish(self) -> None:

        self.value_ticker.put(None)
        if self.displayer.is_alive():
            self.displayer.join()


class Stepper_Dialog(QDialog):

    def __init__(self, title: str, subtitle: Optional[str], initial_step: str,
      number_of_steps: int, minimum_width: Optional[int]) -> None:

        QDialog.__init__(self)
        self.number_of_steps = number_of_steps
        self.current_step = 0

        layout = QVBoxLayout(self)
        self.progress_bar = QProgressBar(self)
        self.progress_bar.setRange(0, self.number_of_steps)
        self.progress_label = QLabel(initial_step)
        self.progress_label.setTextFormat(Qt.TextFormat.PlainText)
        if subtitle is not None:
            label = QLabel(subtitle, self)
            label.setTextFormat(Qt.TextFormat.PlainText)
            layout.addWidget(label)
        layout.addWidget(self.progress_bar)
        layout.addWidget(self.progress_label)

        if minimum_width:
            self.setMinimumSize(minimum_width, self.minimumSize().height())

        self.setWindowTitle(title)
        self.setWindowModality(Qt.WindowModality.NonModal)

    def get_current_step(self) -> int:

        return self.current_step

    def advance_step(self) -> None:

        self.current_step += 1
        self.progress_bar.setValue(self.current_step)

    def set_label(self, txt: str) -> None:

        self.progress_label.setText(txt)

    def reject(self) -> None:

        pass  # suppress standard interpretation of ESC

    def closeEvent(self, event: Optional[QCloseEvent]) -> None:

        if event is not None:
            event.ignore()


class Gui_Stepper(Stepper):

    def __init__(self, steps: Sequence[str], conclude: Optional[str],
      title: str, subtitle: Optional[str], minimum_width: Optional[int]) -> None:

        self.steps = steps
        self.conclude = conclude
        self.setup(title, subtitle, minimum_width)

    @gui.thread
    def setup(self, title: str, subtitle: Optional[str], minimum_width: Optional[int]) -> None:

        self.stepper_dialog = Stepper_Dialog(title, subtitle, self.steps[0] if self.steps else '',
          len(self.steps), minimum_width)
        gui.adjust_and_center(self.stepper_dialog)

    @gui.thread
    def announce(self) -> None:

        previous_value = self.stepper_dialog.get_current_step()
        if previous_value < len(self.steps) or not self.conclude:
            txt = self.steps[previous_value]
        else:
            txt = self.conclude
        self.stepper_dialog.advance_step()
        self.stepper_dialog.show()
        with gui.recentering(self.stepper_dialog):
            self.stepper_dialog.set_label(txt)

    @gui.thread
    def finish(self) -> None:

        self.stepper_dialog.done(0)


class Gui_Interface(User_Interface):

    def __init__(self, qapp: QApplication, app_name: str, title: str, icon_name: str) -> None:

        self.qapp = qapp
        self.app_name = app_name
        self.title = title
        self.icon_name = icon_name

    @gui.thread
    def message(self, txt: str, timeout: Optional[int], title: Optional[str], icon: QMessageBox.Icon | str) -> None:

        dialog = Timeout_Messagebox(txt, timeout, title or self.title, icon)
        dialog.exec()

    def warning(self, txt: str, timeout: Optional[int] = None, title: Optional[str] = None) -> None:

        self.message(txt, timeout, title, QMessageBox.Icon.Warning)

    def critical(self, txt: str, timeout: Optional[int] = None, title: Optional[str] = None) -> None:

        self.message(txt, timeout, title, QMessageBox.Icon.Critical)

    @gui.thread
    def question(self, txt: str, timeout: Optional[int] = None, default: bool = False,
      title: Optional[str] = None) -> bool:

        dialog = Timeout_Questionbox(txt, timeout, title or self.title, default)
        answer = dialog.exec()
        return bool(answer == QMessageBox.StandardButton.Yes)

    def password(self, txt: str, cancellable: bool = False, title: Optional[str] = None) -> Optional[str]:

        return password_dialog.run(title or self.title, txt, cancellable)

    @gui.thread
    def selection(self, heading: str, description: str, descriptions: Sequence[tuple[str, str]],
      values: Sequence[A], default: A, timeout: Optional[int], title: Optional[str] = None) -> A:

        dialog: Timeout_Selectiondialog[A] = Timeout_Selectiondialog(heading, description, descriptions,
          values, default, timeout, title or self.title)
        answer = dialog.run()
        return answer

    def notify(self, txt: str, timeout: Optional[int] = None, title: Optional[str] = None,
      icon_name: Optional[str] = None) -> None:

        title = title or self.title
        icon_name = icon_name or self.icon_name

        success = notification.optimistic_notify(self.app_name, title, txt, icon_name, timeout)
        if not success:
            self.simple_notify(txt, timeout, title, icon_name)

    def simple_notify(self, txt: str, timeout: Optional[int] = None, title: Optional[str] = None,
      icon_name: Optional[str] = None) -> None:

        title = title or self.title
        icon_name = icon_name or self.icon_name

        self.message(txt, timeout, title, icon_name)

    @contextmanager
    def ticker(self, initial_latency: Optional[int] = None, intermediate_latency: Optional[int] = None,
      title: Optional[str] = None, minimum_width: Optional[int] = None) -> Iterator[Ticker]:

        ticker = Gui_Ticker(initial_latency, intermediate_latency, title or self.title, minimum_width)
        try:
            yield ticker
        finally:
            ticker.finish()

    @contextmanager
    def stepper(self, steps: Sequence[str], conclude: Optional[str] = None, title: Optional[str] = None,
      subtitle: Optional[str] = None, minimum_width: Optional[int] = None) -> Iterator[Stepper]:

        stepper = Gui_Stepper(steps, conclude, title or self.title, subtitle, minimum_width)
        try:
            yield stepper
        finally:
            stepper.finish()


class Gui_Application(gui.Application):

    def __init__(self, app_name: str, app_title: str, icon_name: str) -> None:

        gui.Application.__init__(self,
          lambda qapp: Gui_Interface(qapp, app_name, app_title, icon_name), app_name, icon_name)
