
"""Command line user interface."""

# Author: 2012 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at:
#
#   https://spdx.org/licenses/EUPL-1.1.html
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['Cli_Interface']


from typing import Optional, TypeVar
from collections.abc import Sequence, Iterator
from contextlib import contextmanager
import re
import getpass
import sys

from .Abstract_User_Interface import Ticker, Stepper, User_Interface
from .cli import util


A = TypeVar('A')


re_hotkey = re.compile(r'&(.)')

def strip_hotkey_marker(txt: str) -> str:

    return re_hotkey.sub(r'\1', txt)


class Cli_Ticker(Ticker):

    def message(self, txt: str) -> None:

        assert isinstance(txt, str)
        print(txt)


class Cli_Stepper(Stepper):

    def __init__(self, steps: Sequence[str], conclude: Optional[str], subtitle: Optional[str]) -> None:

        assert all(isinstance(step, str) for step in steps)
        assert conclude is None or isinstance(conclude, str)
        assert subtitle is None or isinstance(subtitle, str)

        self.steps = steps
        self.conclude = conclude
        self.current_step = 0

        if subtitle is not None:
            print(subtitle)

    def announce(self) -> None:

        if self.current_step < len(self.steps) or not self.conclude:
            txt = (self.steps[self.current_step] + ' (' + str(self.current_step + 1) + '/'
              + str(len(self.steps)) + ')')
        else:
            txt = self.conclude
        self.current_step += 1
        print(txt)


class Cli_Interface(User_Interface):

    def warning(self, txt: str, timeout: Optional[int] = None, title: Optional[str] = None) -> None:

        print('Warnung: ' + txt, file = sys.stderr)

    def critical(self, txt: str, timeout: Optional[int] = None, title: Optional[str] = None) -> None:

        assert isinstance(txt, str)
        print('Fehler: ' + txt, file = sys.stderr)

    def question(self, txt: str, timeout: Optional[int] = None, default: bool = False,
      title: Optional[str] = None) -> bool:

        assert isinstance(txt, str)
        assert timeout is None or default is not None
        question = 'Frage: ' + txt + ' (j/n) '
        answer = util.line_input(question, timeout = None if timeout is None else timeout / 1000.0,
          accept = lambda s: s in ('j', 'n'))
        return default if answer is None else answer == 'j'

    def password(self, txt: str, cancellable: bool = False, title: Optional[str] = None) -> Optional[str]:

        while True:
            s = getpass.getpass(txt + ' ')
            if s != '' or cancellable:
                return s

    def selection(self, heading: str, description: str, descriptions: Sequence[tuple[str, str]],
      values: Sequence[A], default: A, timeout: Optional[int], title: Optional[str] = None) -> A:

        assert all(len(description[0]) == 1 for description in descriptions)
        assert default in values

        selectors = [selector for selector, _ in descriptions]
        print(heading)
        print()
        print(description)
        print()
        for index, (selector, description) in enumerate(descriptions):
            print('  ({}) {} {}'.format(selector, '*' if values[index] == default else ' ',
              strip_hotkey_marker(description)))
        print()
        answer = util.line_input('Auswahl? ', timeout = None if timeout is None else timeout / 1000.0,
          accept = lambda s: s == '' or s in selectors)

        return default if answer is None or answer == '' else values[selectors.index(answer)]

    def notify(self, txt: str, timeout: Optional[int] = None, title: Optional[str] = None,
      icon_name: Optional[str] = None) -> None:

        assert isinstance(txt, str)
        print('Hinweis: ' + txt, file = sys.stderr)

    @contextmanager
    def ticker(self, initial_latency: Optional[int] = None, intermediate_latency: Optional[int] = None,
      title: Optional[str] = None, minimum_width: Optional[int] = None) -> Iterator[Ticker]:

        yield Cli_Ticker()

    @contextmanager
    def stepper(self, steps: Sequence[str], conclude: Optional[str] = None, title: Optional[str] = None,
      subtitle: Optional[str] = None, minimum_width: Optional[int] = None) -> Iterator[Stepper]:

        yield Cli_Stepper(steps, conclude, subtitle)
