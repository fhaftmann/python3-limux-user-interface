
"""Frozen exceptions as explicit values."""

# Author: 2012 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at:
#
#   https://spdx.org/licenses/EUPL-1.1.html
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['Result', 'Value', 'produce', 'freeze', 'thaw', 'Result']


from typing import NoReturn, TypeAlias, TypeVar, ParamSpec, Generic
from collections.abc import Callable
from dataclasses import dataclass


A = TypeVar('A')
P = ParamSpec('P')


def produce(E: Callable[P, BaseException], *args: P.args, **kwargs: P.kwargs) -> NoReturn:
    raise E(*args, **kwargs)


@dataclass(frozen = True)
class Value(Generic[A]):
    value: A

@dataclass(frozen = True)
class Frozen_Exception(Generic[A]):
    exception: BaseException

Result: TypeAlias = Value[A] | Frozen_Exception[A]


def freeze(f: Callable[P, A], *args: P.args, **kwargs: P.kwargs) -> Result[A]:

    try:
        result = f(*args, **kwargs)
        return Value(result)
    except BaseException as e:
        return Frozen_Exception(exception = e)


def thaw(result: Result[A]) -> A:

    match result:
        case Value(value):
            return value
        case Frozen_Exception(exception):
            raise exception.with_traceback(exception.__traceback__)
