
"""Thread-safe singleton value ticker with latency."""

# Author: 2013 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at:
#
#   https://spdx.org/licenses/EUPL-1.1.html
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['Value_Ticker']


from typing import TypeVar, Generic, Optional
from threading import Condition, Event, Lock


A = TypeVar('A')


class Value_Ticker(Generic[A]):

    """Singleton value ticker safe for one producer and one consumer.
    Existing value is discarded when a new value is put.
    Putting None finishes tickering."""

    def __init__(self) -> None:

        self.available = Condition(Lock())
        self.finished = Event()
        self.value: Optional[A] = None

    def get(self, latency: float) -> Optional[A]:

        if self.finished.wait(latency):
            return None
        else:
            with self.available:
                if self.finished.is_set():
                    return None
                else:
                    self.available.wait()
                    x = self.value
                    return x

    def put(self, x: A) -> None:

        if not self.finished.is_set():
            with self.available:
                if x is None:
                    self.finished.set()
                self.value = x
                self.available.notify()
