
"""Abstract interface for common user interaction gestures."""

# Author: 2012 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at:
#
#   https://spdx.org/licenses/EUPL-1.1.html
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['User_Interface', 'Ticker', 'Stepper']


from typing import Optional, TypeVar
from collections.abc import Sequence
from contextlib import AbstractContextManager


A = TypeVar('A')


class Ticker:

    def message(self, txt: str) -> None:

        raise NotImplementedError


class Stepper:

    def announce(self) -> None:

        raise NotImplementedError


class User_Interface:

    def warning(self, txt: str, timeout: Optional[int] = None, title: Optional[str] = None) -> None:

        raise NotImplementedError

    def critical(self, txt: str, timeout: Optional[int] = None, title: Optional[str] = None) -> None:

        raise NotImplementedError

    def question(self, txt: str, timeout: Optional[int] = None, default: bool = False,
      title: Optional[str] = None) -> bool:

        raise NotImplementedError

    def password(self, txt: str, cancellable: bool = False, title: Optional[str] = None) -> Optional[str]:

        raise NotImplementedError

    def selection(self, heading: str, description: str, descriptions: Sequence[tuple[str, str]],
      values: Sequence[A], default: A, timeout: Optional[int], title: Optional[str] = None) -> A:

        raise NotImplementedError

    def notify(self, txt: str, timeout: Optional[int] = None, title: Optional[str] = None,
      icon_name: Optional[str] = None) -> None:

        raise NotImplementedError

    def ticker(self, initial_latency: Optional[int] = None, intermediate_latency: Optional[int] = None,
      title: Optional[str] = None, minimum_width: Optional[int] = None) -> AbstractContextManager[Ticker]:

        raise NotImplementedError

    def stepper(self, steps: Sequence[str], conclude: Optional[str] = None, title: Optional[str] = None,
      subtitle: Optional[str] = None, minimum_width: Optional[int] = None) -> AbstractContextManager[Stepper]:

        raise NotImplementedError
