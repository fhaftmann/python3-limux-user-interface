
"""Abstraction layer for user interfaces with backends for console and Qt."""

# Author: 2012 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at:
#
#   https://spdx.org/licenses/EUPL-1.1.html
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['User_Interface', 'dual_user_interface', 'long_description']


from .Abstract_User_Interface import User_Interface
from . import dual_user_interface


long_description = """
    This library provides an abstraction layer for user interface gestures
    such that they can be used uniformly for console and graphical UI.
"""
