
"""Minimalistic browser window."""

# Author: 2016 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at:
#
#   https://spdx.org/licenses/EUPL-1.1.html
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['run']


from typing import Optional

from PyQt5.QtCore import QUrl
from PyQt5.QtGui import QContextMenuEvent
from PyQt5.QtWidgets import QDialog, QVBoxLayout, QPushButton
from PyQt5.QtWebEngineWidgets import QWebEngineView

from . import base as gui
from ..Gui_Interface import Gui_Interface


dialog_size = (796, 576)  # fixed size to fit on small screens including window decoration


class Browser(QWebEngineView):

    def contextMenuEvent(self, event: Optional[QContextMenuEvent]) -> None:
        pass


class Dialog(QDialog):

    def __init__(self, title: str, url: str) -> None:

        QDialog.__init__(self)
        self.setWindowTitle(title)
        self.resize(*dialog_size)

        verticalLayout = QVBoxLayout(self)
        self.setLayout(verticalLayout)
        verticalLayout.setSpacing(6)
        verticalLayout.setContentsMargins(0, 0, 0, 0)

        webView = Browser(self)
        webView.setUrl(QUrl(url))
        verticalLayout.addWidget(webView)

        closeButton = QPushButton(self)
        closeButton.setText('Fenster &schließen')
        closeButton.clicked.connect(self.close)
        verticalLayout.addWidget(closeButton)


@gui.thread
def run(ui: Gui_Interface, url: str) -> None:

    dialog = Dialog(ui.title, url)
    gui.adjust_and_center(dialog)
    dialog.exec()
