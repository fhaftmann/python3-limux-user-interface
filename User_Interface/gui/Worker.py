
"""Functional combination of a worker thread and a dialog with abort/close button."""

# Author: 2013 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at:
#
#   https://spdx.org/licenses/EUPL-1.1.html
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the Licence for the specific language governing
# permissions and limitations under the Licence.


from __future__ import annotations


__all__ = ['Worker_Dialog', 'Worker', 'Worker_Application']


from typing import Optional, Type, TypeVar, ParamSpec, Concatenate, Generic, NoReturn
from types import FrameType
from collections.abc import Callable
import threading
import os
import signal

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QShowEvent, QCloseEvent
from PyQt5.QtWidgets import QDialog, QPushButton

from .. import User_Interface
from ..util import exception
from . import base as gui
from ..Gui_Interface import Gui_Interface, Gui_Application


A = TypeVar('A')
P = ParamSpec('P')


class Worker_Dialog(QDialog):

    # initialisation

    def __init__(self, ui: Gui_Interface, worker: Worker[A]) -> None:

        QDialog.__init__(self)
        self.worker = worker
        self.shown = False
        self.closing = False
        self.concluded = False
        self.button = QPushButton('Abbrechen')
        self.button.clicked.connect(self.dismiss)
        self.button.setDefault(True)

        self.setWindowTitle(ui.title)
        self.setWindowModality(Qt.WindowModality.NonModal)

    # gui events

    def showEvent(self, event: Optional[QShowEvent]) -> None:

        if self.shown:
            return
        self.shown = True
        self.worker.start()

    def closeEvent(self, event: Optional[QCloseEvent]) -> None:

        if event is not None:
            event.ignore()
        self.closing = True
        self.worker.abort()
        self.dismiss()

    def dismiss(self) -> None:

        if self.concluded:
            self.accept()
        else:
            self.button.setEnabled(False)
            self.worker.abort()

    def reject(self) -> None:

        self.dismiss()  # suppress standard interpretation of ESC

    # interface for worker

    def conclude(self) -> None:

        self.concluded = True
        if self.closing:
            self.accept()
        else:
            self.button.setText('OK')
            self.button.setEnabled(True)


class Worker(Generic[A]):

    # initialisation

    def __init__(self, Dialog: type[Worker_Dialog], ui: Gui_Interface,
      f: Callable[Concatenate[Worker, Worker_Dialog, P], A], *args: P.args, **kwargs: P.kwargs) -> None:  # type: ignore

        self.f = f
        self.args = args
        self.kwargs = kwargs
        self.executor = threading.Thread(target = self.go)
        self.aborted = threading.Event()
        self.dialog = gui.thread.schedule(Dialog, ui, self)

    # interface for worker dialog

    def start(self) -> None:

        self.executor.start()

    def abort(self) -> None:

        self.aborted.set()

    # internal worker thread

    def go(self) -> None:

        self.result: exception.Result[A] = \
          exception.freeze(self.f, self, self.dialog, *self.args, **self.kwargs)
        gui.thread.schedule(self.dialog.conclude)

    # interface for user

    @gui.thread
    def run(self) -> A:

        gui.adjust_and_center(self.dialog)
        self.dialog.exec()
        self.executor.join()  # formal join on worker thread
        result = exception.thaw(self.result)
        return result

    @classmethod
    def work(Worker, Dialog: Type[Worker_Dialog], ui: Gui_Interface,
      f: Callable[Concatenate[Worker[A], Worker_Dialog, P], A], *args: P.args, **kwargs: P.kwargs) -> A:

        self = Worker(Dialog, ui, f, *args, **kwargs)
        return self.run()


class Worker_Application(Worker[int]):

    @classmethod
    def execute(Application, app_name: str, app_title: str, app_icon: str,
      Dialog: Type[Worker_Dialog],
      f: Callable[Concatenate[User_Interface, Worker[int], Worker_Dialog, P], int],
      *args: P.args, **kwargs: P.kwargs) -> NoReturn:

        main_pid = os.getpid()
        forked_pid = os.fork()
        exit_code: int

        def exit(signum: int, frame: Optional[FrameType]) -> NoReturn:
            raise SystemExit(0)

        signal.signal(signal.SIGUSR1, exit)

        if forked_pid == 0:

            def exec(worker: Worker_Application, dialog: Worker_Dialog, ui: User_Interface) -> int:
                returncode = f(ui, worker, dialog, *args, **kwargs)
                if returncode == 0:
                    os.kill(main_pid, signal.SIGUSR1)
                return returncode

            def run(ui: Gui_Interface) -> int:
                return Application.work(Dialog, ui, exec, ui)  # type: ignore

            app = Gui_Application(app_name, app_title, app_icon)
            exit_code = app.event_loop(run)

            raise SystemExit(exit_code)

        _, status = os.waitpid(forked_pid, 0)
        exit_code = status // 256

        raise SystemExit(exit_code)
