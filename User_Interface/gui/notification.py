
"""Issuing desktop notifications via dbus."""

# Author: 2017 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at:
#
#   https://spdx.org/licenses/EUPL-1.1.html
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['notify', 'optimistic_notify']


from typing import Optional
import pydbus  # type: ignore


dbus_connection_env = 'DBUS_SESSION_BUS_ADDRESS'
bus_name = 'org.freedesktop.Notifications'
object_path = '/org/freedesktop/Notifications'
interface_name = bus_name
fake_no_timeout = 2 ** 31 - 1


def notify(app_name: str, title: str, txt: str, icon_name: str, timeout: Optional[int] = None) -> None:

    session_bus = pydbus.SessionBus()
    notification = session_bus.get(bus_name, object_path)

    notification.Notify(app_name, 0, icon_name, title, txt, [], {},
      timeout if timeout is not None else fake_no_timeout)


def optimistic_notify(app_name: str, title: str, txt: str, icon_name: str, timeout: Optional[int] = None) -> bool:

    # unclear how to import that exception directly, hence we simulate
    try:
        pydbus.connect('')
    except Exception as e:
        Dbus_Connection_Exception = type(e)

    try:
        notify(app_name, title, txt, icon_name, timeout)
        return True
    except Dbus_Connection_Exception:
        return False
