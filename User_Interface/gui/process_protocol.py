
'''Running processes with output to a text window with possible abort.'''

# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at:
#
#   https://spdx.org/licenses/EUPL-1.1.html
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['Process_Protocol', 'Process_Queue']


from typing import Optional
from collections.abc import Sequence
import threading
from queue import Queue
import time
import subprocess

from PyQt5.QtCore import QSize
from PyQt5.QtWidgets import QTextEdit, QVBoxLayout
from PyQt5.QtGui import QFont

from ..Gui_Interface import Gui_Interface
from . import base as gui
from .Worker import Worker, Worker_Dialog


class Process_Protocol(Worker_Dialog):

    def __init__(self, ui: Gui_Interface, worker: Worker[str]) -> None:

        Worker_Dialog.__init__(self, ui, worker)
        layout = QVBoxLayout(self)

        self.protocol = QTextEdit()
        self.protocol.setMinimumSize(QSize(800, 400))
        self.protocol.adjustSize()
        self.protocol.setReadOnly(True)
        self.protocol.setAcceptRichText(False)
        layout.addWidget(self.protocol)

        layout.addWidget(self.button)

    def set_monospace(self) -> None:

        font = QFont('monospace')
        font.setStyleHint(QFont.StyleHint.TypeWriter)
        self.protocol.setFont(font)

    @gui.thread
    def announce_protocol(self, msg: str) -> None:

        self.protocol.append(msg)


class Process_Queue:

    def __init__(self, aborted: threading.Event, cmd: str, args: Sequence[str]) -> None:

        self.queue: Queue[Optional[str]] = Queue()
        self.proc = subprocess.Popen([cmd, *args],
          stdout = subprocess.PIPE, stderr = subprocess.STDOUT, text = True)
        assert self.proc.stdout is not None
        self.reader = self.proc.stdout
        self.aborted = aborted
        self.sentinels = 2
        for thread in [
          threading.Thread(daemon = True, target = self.consume_stdout),
          threading.Thread(daemon = True, target = self.await_proc),
          threading.Thread(daemon = True, target = self.check_abort)]:
            thread.start()

    def get(self) -> Optional[str]:

        some_line = self.queue.get()
        if some_line is not None:
            return some_line
        self.sentinels -= 1
        if self.sentinels == 0:
            return None
        return self.get()

    def returncode(self) -> int:

        return self.proc.wait()

    def consume_stdout(self) -> None:

        try:
            while True:
                line = self.reader.readline()
                if line == '':
                    break
                self.queue.put(line.rstrip('\n'))
                time.sleep(0)  # hand over remaining time slice to other threads
        finally:
            self.reader.close()
            self.queue.put(None)

    def await_proc(self) -> None:

        self.proc.wait()
        self.queue.put(None)

    def check_abort(self) -> None:

        self.aborted.wait()
        self.proc.terminate()
