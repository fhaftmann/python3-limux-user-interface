
"""Minimalistic password dialog."""

# Author: 2016 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at:
#
#   https://spdx.org/licenses/EUPL-1.1.html
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['run']


from typing import Optional

from PyQt5 import QtWidgets

from . import base as gui


class Password_Dialog(QtWidgets.QDialog):

    def __init__(self, title: str, txt: str, cancellable: bool) -> None:

        QtWidgets.QDialog.__init__(self)

        self.cancellable = cancellable

        self.setWindowTitle(title)

        self.main_layout = QtWidgets.QVBoxLayout(self)

        self.label = QtWidgets.QLabel(txt, parent = self)
        self.main_layout.addWidget(self.label)

        self.password_line = QtWidgets.QLineEdit(self)
        self.password_line.setEchoMode(QtWidgets.QLineEdit.EchoMode.Password)
        self.main_layout.addWidget(self.password_line)

        self.button_layout = QtWidgets.QHBoxLayout()

        self.button_ok = QtWidgets.QPushButton('Weiter', self)
        self.button_ok.clicked.connect(self.handle_ok)
        self.button_layout.addWidget(self.button_ok)

        if self.cancellable:
            self.button_cancel = QtWidgets.QPushButton('Abbrechen', self)
            self.button_cancel.clicked.connect(self.reject)
            self.button_layout.addWidget(self.button_cancel)

        self.main_layout.addLayout(self.button_layout)

        self.password: Optional[str] = None

    def handle_ok(self) -> None:

        s = self.password_line.text()
        if s != "":
            self.password = self.password_line.text()
            self.accept()
        elif self.cancellable:
            self.reject()

    def reject(self) -> None:

        if self.cancellable:
            super(Password_Dialog, self).reject()  # standard interpretation of ESC only iff cancellable


@gui.thread
def run(title: str, txt: str, cancellable: bool) -> Optional[str]:

    dialog = Password_Dialog(title, txt, cancellable)
    gui.adjust_and_center(dialog)
    dialog.exec()
    return dialog.password
