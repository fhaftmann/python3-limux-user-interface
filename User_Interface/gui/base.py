
"""Runtime administration of Qt-based applications with workflow under control
of program logic rather than interface."""

# Author: 2012 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at:
#
#   https://spdx.org/licenses/EUPL-1.1.html
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['Access_Exception', 'thread', 'Application', 'adjust_and_center', 'recentering']


from typing import TypeVar, ParamSpec, Concatenate, Any, cast
from collections.abc import Callable, Iterator
from contextlib import contextmanager
import functools

import _thread as threadcontrol
from threading import Thread
from queue import Queue
import signal

from PyQt5.QtCore import QObject, QTimer, pyqtSignal
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication, QDialog

from ..util import exception


A = TypeVar('A')
B = TypeVar('B')
P = ParamSpec('P')


class Access_Exception(Exception):

    """Access to GUI outside of GUI event loop."""


class Thread_Access(QObject):

    """Control flow broker between GUI thread and non-GUI threads."""

    notification = pyqtSignal()

    def __init__(self) -> None:

        super(Thread_Access, self).__init__()
        self.calls: Queue[tuple[int, tuple[Callable[..., Any], Any, Any]]] = Queue(maxsize = 1)
        self.results: dict[int, Any] = {}
        self.notification.connect(self.process)

    def intercept(self) -> None:

        # must only be called after end of gui event loop
        # and makes still existing worker thread interaction abort
        for ident in self.results:
            self.results[ident].put(exception.freeze(exception.produce, Access_Exception))

    def schedule(self, f: Callable[P, A], *args: P.args, **kwargs: P.kwargs) -> A:

        # ensure thread-specific result queue
        ident = threadcontrol.get_ident()
        if ident not in self.results:
            self.results[ident] = Queue(maxsize = 1)

        # schedule call
        self.calls.put((ident, (f, args, kwargs)))
        self.notification.emit()
        return cast(A, exception.thaw(self.results[ident].get()))

    def process(self) -> None:

        ident, (f, args, kwargs) = self.calls.get()
        result = exception.freeze(f, *args, **kwargs)
        self.results[ident].put(result)

    def __call__(self, f: Callable[P, A]) -> Callable[P, A]:

        @functools.wraps(f)
        def f_(*args: P.args, **kwargs: P.kwargs) -> A:
            return self.schedule(f, *args, **kwargs)

        return f_

thread = Thread_Access()


class Application:

    def __init__(self, init_interface: Callable[[QApplication], B],
      app_name: str, icon_name: str) -> None:

        QApplication.setSetuidAllowed(True)
        self.qapp = QApplication([app_name])
        self.qapp.setQuitOnLastWindowClosed(False)

        self.qapp.setWindowIcon(QIcon.fromTheme(icon_name))

        self.ui = init_interface(self.qapp)

    def event_loop(self, f: Callable[Concatenate[B, P], A],
      *args: P.args, **kwargs: P.kwargs) -> A:

        # signaling to start worker thread not before gui event loop is running
        running: Queue[tuple[()]] = Queue(maxsize = 1)
        start_timer = QTimer()
        start_timer.setSingleShot(True)
        start_timer.timeout.connect(lambda: running.put(()))
        start_timer.start(0)

        # worker thread result as last value of list
        result: list[exception.Result[A]] = \
          [exception.freeze(exception.produce, ValueError)]
            # formal fallback value on extraordinary exception  # noqa: E116

        # wrapper for worker thread
        def run_worker_thread(*args: P.args, **kwargs: P.kwargs) -> None:
            running.get()  # do not start before gui event loop is running
            result.append(exception.freeze(f, self.ui, *args, **kwargs))
            self.qapp.quit()  # quit gui event loop if worker thread finishes

        # start worker thread
        worker_thread = Thread(target = run_worker_thread, args = args, kwargs = kwargs)
        worker_thread.start()

        # prevent keyboard interrupt during event loop
        handler = signal.signal(signal.SIGINT, signal.SIG_IGN)

        # ensurance if event loop is aborted abnormally
        self.qapp.aboutToQuit.connect(thread.intercept)

        # run gui event loop
        self.qapp.exec()

        # wait for worker thread to be finished after gui event loop has shut down
        worker_thread.join()

        # restore previous signal handler
        signal.signal(signal.SIGINT, handler)

        # return result of worker thread
        return exception.thaw(result.pop())


def adjust_and_center(dialog: QDialog) -> None:

    dialog.adjustSize()
    primary_screen = QApplication.primaryScreen()
    if primary_screen is not None:
        dialog.move(primary_screen.availableGeometry().center() - dialog.frameGeometry().center())


@contextmanager
def recentering(dialog: QDialog) -> Iterator[None]:

    rectangle_before = dialog.frameGeometry()
    yield
    rectangle_after = dialog.frameGeometry()
    if rectangle_before != rectangle_after:
        pos_x = rectangle_after.left() + (rectangle_before.width() - rectangle_after.width()) // 2
        pos_y = rectangle_after.top() + (rectangle_before.height() - rectangle_after.height()) // 2
        dialog.move(pos_x, pos_y)
