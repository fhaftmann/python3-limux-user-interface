
# Purpose

Package *@{control Source}* provides various enhancements
over toolkit-based user interface gestures.  This sounds a little bit
like »grand-unified everything«, and indeed the package consists
of various parts which, although always committed to user-interface
issues, are technically independent and just combined into one package
for pragmatic reasons.  The covered issues themselves are:

* [Safe multi-threaded gui
  programming](#safe-multi-threaded-gui-programming) – technically,
  the biggest contribution.

* A set of [common user interaction
  gestures](#common-user-interaction-gestures) (warning, error,
  question, progress indicator, …) implemented uniformly for
  graphical and command-line interfaces.  The gestures for graphical
  interfaces enable control-flow-oriented rather than event-oriented
  gui programming.  This uniform interface also provides some
  abstraction over the underlying toolkit such that it is possible
  to replace it by a different implementation if this should turn
  out necessary in the future.  The current implementation uses Qt5.

* A [»worker« dialog](#worker-dialog) for controlled reporting of
  background process output with possibility for the user to abort.


# Safe Multi-threaded Gui Programming

## Concept

When using a gui toolkit on top of an existing programming environment
(e.g. Qt5 on top of Python), the very architecture of the toolkit
usually rules out to use any multi-threading facilities of the
programming environment.  Instead the toolkit provides its own set
of threads, inter-thread communication facilities, locks etc. which
you are supposed to use instead.  We provide an architecture which
enables the system developer to use Python's native thread support,
thus also including any (available or future) libraries building upon
Python threads, roughly following
<https://www.threadingbuildingblocks.org/docs/help/tbb_userguide/Design_Patterns/GUI_Thread.html>.
This is achieved as follows:

* There is one distinguished thread, the *gui thread*.  Its purpose
  is to listen to gui user events, showing dialogs etc.  From the
  perspective of the toolkit, this is »the« thread.

* There is a second distinguished thread, the *main worker thread*.
  It implements the main control flow of the application.  It is not
  allowed to interact with gui elements directly (see next point),
  but can schedule arbitrary additional threads etc.

* A *control flow broker* connects gui thread and main worker thread
  as follows:

    * When starting the gui event loop, the main worker thread is
      started simultaneously in the background.

    * When the main worker thread finishes, the gui event loop
      shuts down.

    * Non-gui threads may schedule computations for the gui thread.
      The control flow broker uses mechanisms of the toolkit to run
      this computation inside the gui thread and hand back its result
      to the scheduling non-gui thread synchronously.

The consequences are:

* The schedule interface is pythonic, using decorators etc. instead
  of C++- or Java-like patterns.

* The schedule interface is functional – issuing a schedule request
  is almost as simple as calling a function directly.

* There is no need to poll gui events in the main worker thread –
  the gui thread is always reactive unless gui events or scheduled
  computations are processed.  If scheduled computations are kept
  to the bare minimum (i.e. creating and accessing gui elements),
  they are fast to execute and do not block the gui event loop.

To make this work out smoothly, adhere to the following:

* Keep the gui thread free of unnecessary things.  Interfering with
  the gui thread should only be necessary when creating or accessing
  gui elements.  Everything else, particularly costly things like heavy
  computation, external processes, long-enduring actions etc. should
  happen in one of the non-gui threads.

* The control flow broker provides no means to schedule non-gui threads
  from the gui thread.  This is deliberate since that would violate
  the principle that the control flow is under the authority of the
  main worker thread (which of course may delegate parts of it to
  other non-gui threads).  So the idea is that any gui events which
  have an effect on the application logic store event indicators in
  the gui data structures which on occasion are fetched by one of
  the non-gui threads.

    As example, take a modal dialog: it displays a certain notification
  to the user with the request to insert or adjust some values.
  Inside this framework it is most convenient to treat the modal
  dialog as a function: it accepts certain values (default values,
  variable text etc.)  and returns result values.  Hence it is best if
  the class which implements that dialog provides such a function which

    * initializes the dialog appropriately;

    * displays the dialog;

    * queries the dialog elements afterward to return the appropriate
    values.

    This function then can just be called by any non-gui thread
  through the control flow broker, providing a perfect functional
  abstraction over the dialog.

* Refrain from accessing gui elements after their destruction,
  e.g. after a dialog has been closed.  This may yield unpredictable
  results, including blocking the whole application.


## Code Interfaces

Concerning the control flow broker:

*@{function gui.base.Application in User_Interface}*

:   Singleton class constructor.  Must be instantiated exactly once
    for a gui application.

    *@{argument init_interface of User_Interface.gui.base.Application}*

    :   User-provided initialization function.  Receives a
        *QApplication* object and may return an arbitrary value which
        is later on passed to the main worker thread by means of
        *@{identname gui.base.Application.event_loop in User_Interface}*.
        The most simple argument here is just the identity function.

    *@{argument app_name of User_Interface.gui.base.Application}*

    :   Formal internal name of the application.

    *@{argument icon_name of User_Interface.gui.base.Application}*

    :   A default icon name, referring to an
        icon according to the [XDG Icon Theme
        Specification](http://freedesktop.org/wiki/Specifications/icon-theme-spec/).

    returns

    :   Singleton instance of @{identname gui.base.Application in
        User_Interface}.

*@{function gui.base.Application.event_loop in User_Interface}*

:   Run event loop and main worker thread.

    *@{argument f of User_Interface.gui.base.Application.event_loop}*

    :   The implementation of the main worker thread.  As arguments
        it gets the result of the *@{argument init_interface of
        User_Interface.gui.base.Application}* call in the constructor
        call of *@{identname gui.base.Application in User_Interface}*
        plus additional parameters given to *@{identname event_loop
        in User_Interface.gui.base.Application}*.

    returns

    :   Whatever *@{argument f of
        User_Interface.gui.base.Application.event_loop}* returns.
        Exceptions during processing of *@{argument f of
        User_Interface.gui.base.Application.event_loop}* are propagated.

*@{identname gui.base.thread in User_Interface}*

:   The control flow broker.

    *@{function gui.base.thread.schedule in User_Interface}*

    :   Schedule a computation *@{argument f of
        User_Interface.gui.base.thread.schedule}* with (optional) arguments
        to the gui thread and wait synchronously until this finishes.

        *@{argument f of User_Interface.gui.base.thread.schedule}*

        :   The computation to schedule.

        returns

        :   Whatever *@{argument f of User_Interface.gui.base.thread.schedule}*
            returns.  Exceptions during processing of *@{argument f of
            User_Interface.gui.base.thread.schedule}* are propagated.

    *@{function gui.base.thread.\_\_call\_\_ in User_Interface}*

    :   Decorator.  Functions decorated with *@@{identname gui.base.thread
        in User_Interface}* are implicitly scheduled to the gui thread
        on invocation.

Auxiliary:

*@{function gui.base.adjust_and_center in User_Interface}*

*@{function gui.base.recentering in User_Interface}*



# Common User Interaction Gestures

<!-- no undesired event paths (e.g. closing a dialog) -->

*@{function Gui_Interface.Gui_Application in User_Interface}*

*@{function Gui_Interface.Gui_Interface in User_Interface}*

*@{function Cli_Interface.Cli_Interface in User_Interface}*

*@{function dual_user_interface.run_with in User_Interface}*

*@{function User_Interface.warning in User_Interface.Abstract_User_Interface}*

*@{function User_Interface.critical in User_Interface.Abstract_User_Interface}*

*@{function User_Interface.question in User_Interface.Abstract_User_Interface}*

*@{function User_Interface.password in User_Interface.Abstract_User_Interface}*

*@{function User_Interface.selection in User_Interface.Abstract_User_Interface}*

*@{function User_Interface.notify in User_Interface.Abstract_User_Interface}*

*@{function User_Interface.ticker in User_Interface.Abstract_User_Interface}*

*@{function User_Interface.stepper in User_Interface.Abstract_User_Interface}*


# Desktop Notifications

*@{executable notify in debian/python3-dual-user-interface/usr/bin}*:

@#{argspec User_Interface.cmd.notify.parser for debian/python3-dual-user-interface/usr/bin/notify}


# Password requests

*@{executable supply-password in debian/python3-dual-user-interface/usr/bin}*:

@#{argspec User_Interface.cmd.supply_password.parser for debian/python3-dual-user-interface/usr/bin/supply-password}


# Minimal browser

*@{executable minimal-browser in debian/python3-dual-user-interface/usr/bin}*:

@#{argspec User_Interface.cmd.minimal_browser.parser for debian/python3-dual-user-interface/usr/bin/minimal-browser}


# Interruptible process protocol

*@{executable process-protocol in debian/python3-dual-user-interface/usr/bin}*:

@#{argspec User_Interface.cmd.process_protocol.parser for debian/python3-dual-user-interface/usr/bin/process-protocol}


# Worker Dialog

*@{function Worker_Dialog in User_Interface.gui.Worker}*

*@{function Worker in User_Interface.gui.Worker}*

*@{function Worker.run in User_Interface.gui.Worker}*

*@{function Worker.work in User_Interface.gui.Worker}*
