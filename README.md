
dual-user-interface
===================

This library provides provides an abstraction layer for user interface gestures
such that they can be used uniformly for console and graphical UI.
